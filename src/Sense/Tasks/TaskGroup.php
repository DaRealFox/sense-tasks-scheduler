<?php
namespace Sense\Tasks;

/**
 * Class TaskGroup
 *
 * @package Sense\Tasks
 */
class TaskGroup
{
    public function __construct(array $tasks = [])
    {
       $this->tasks = $tasks;
    }

    /**
     * @return Task[]
     */
    public function getTasks()
    {
        return $this->tasks;
    }
}