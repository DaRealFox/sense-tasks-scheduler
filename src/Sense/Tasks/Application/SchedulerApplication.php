<?php
namespace Sense\Tasks\Application;

use Sense\Tasks\Application\Command\RunCommand;
use Sense\Tasks\Application\Command\TaskListCommand;
use Symfony\Component\Console\Application;

/**
 * Class SchedulerApplication
 * @package Sense\Tasks\Application
 */
class SchedulerApplication extends Application
{
    const NAME = 'SENSE Scheduler';
    const VERSION = '1.0';

    /**
     * @var string
     */
    private $configFile;

    public function __construct() {
        parent::__construct(self::NAME, self::VERSION);

        # Add the commands
        $this->add(new RunCommand());
        $this->add(new TaskListCommand());
    }

    /**
     * @param $file
     */
    public function setConfigFile($file)
    {
        $this->configFile = $file;
    }

    /**
     * @return string
     */
    public function getConfigFile() {
        return $this->configFile ?: $this->getDefaultConfigFile();
    }

    /**
     * @return string
     */
    public function getDefaultConfigFile()
    {
        return realpath(__DIR__ . '/../crontab.yaml');
    }
}