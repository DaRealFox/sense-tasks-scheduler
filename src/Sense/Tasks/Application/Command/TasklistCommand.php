<?php
namespace Sense\Tasks\Application\Command;

use DateTime;

use Sense\Tasks\LazyTask;
use Symfony\Component\Console\Helper\TableHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TaskListCommand extends AbstractCommand
{
    protected function configure()
    {
        $this->setName('tasklist');
        $this->addOption('config', 'c', InputOption::VALUE_REQUIRED, 'The task runner configuration file.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        # Get the scheduler and check for tasks
        $tasks = $this->getTaskList($input->getOption('config'))->getTasks();
        $table = $this->getHelper('table');

        /** @var TableHelper $table */
        $table->setHeaders(['Task', 'Schedule', 'Class', 'Next Run']);

        foreach($tasks as $taskName => $task) {
            $taskClass = get_class($task->task);

            if($task->task instanceof LazyTask) {
                $taskClass = get_class($task->task->getTask());
            }

            $table->addRow([$taskName, (string) $task->schedule, $taskClass, $task->getNextRunTime()->format('c')]);
        }

        $table->render($output);

        return 0;
    }
}