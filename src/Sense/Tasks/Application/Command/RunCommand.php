<?php
namespace Sense\Tasks\Application\Command;

use DateTime;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RunCommand extends AbstractCommand
{
    protected function configure()
    {
        $this->setName('run');
        $this->addOption('date', 'd', InputOption::VALUE_REQUIRED, 'The execution date.', 'now');
        $this->addOption('config', 'c', InputOption::VALUE_REQUIRED, 'The task runner configuration file.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        # Get the scheduler and check for tasks
        $this->getScheduler($input->getOption('config'))
             ->runTasksDueAt(new DateTime($input->getOption('date')));

        return 0;
    }
}