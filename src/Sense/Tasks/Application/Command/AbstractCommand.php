<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 27.09.2015
 * Time: 22:56
 */

namespace Sense\Tasks\Application\Command;

use Sense\Tasks\Application\SchedulerApplication;
use Sense\Tasks\Scheduler\Scheduler;
use Sense\Tasks\Scheduler\TaskList\YamlTaskList;
use Symfony\Component\Console\Command\Command;

class AbstractCommand extends Command
{
    /**
     * @param null $config
     * @return Scheduler
     */
    protected function getScheduler($config = null)
    {
        # Create configured scheduler instance
        return new Scheduler($this->getTaskList($config));
    }

    /**
     * @param string $config
     * @return YamlTaskList
     */
    protected function getTaskList($config = null)
    {
        return new YamlTaskList($config ?: $this->getApplication()->getConfigFile());
    }

    /**
     * @return SchedulerApplication
     */
    public function getApplication() {
        return parent::getApplication();
    }
}