<?php
namespace Sense\Tasks\Runner;


use Sense\Tasks\TaskGroup;
use Sense\Tasks\TaskRunner;

class ParallelTaskRunner implements TaskRunner
{

    /**
     * @param TaskGroup $taskGroup
     */
    public function runTasks(TaskGroup $taskGroup)
    {
        $processes = [];

        foreach($taskGroup->getTasks() as $task) {
            # Fork the current process
            $pid = pcntl_fork();

            if($pid === -1) {
                echo "Error forking process. Exiting.\n";
                exit(2);
            }

            if($pid > 0) {
                # We are the parent, continue to next task
                $processes[$pid] = $task;
                continue;
            }

            try {
                $task->run();
            }
            catch(\Exception $e) {
                fwrite(STDERR, $e->getMessage() . "\n");
                exit(1);
            }

            exit(0);
        }

        while(($pid = pcntl_waitpid(0, $status)) !== -1) {
            $return = pcntl_wexitstatus($status);

            if($return !== 0) {
                fwrite(STDERR, "Process $pid exited with errors.\n");
            }
        }
    }
}