<?php
namespace Sense\Tasks\Runner;


use Sense\Tasks\TaskGroup;
use Sense\Tasks\TaskRunner;

class SequentialTaskRunner implements TaskRunner
{

    /**
     * @param TaskGroup $taskGroup
     */
    public function runTasks(TaskGroup $taskGroup)
    {
        foreach($taskGroup->getTasks() as $task) {
            $task->run();
        }
    }
}