<?php
namespace Sense\Tasks;

interface TaskRunner
{
    public function runTasks(TaskGroup $taskGroup);
}