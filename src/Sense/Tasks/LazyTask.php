<?php
namespace Sense\Tasks;

use ReflectionClass;

class LazyTask implements Task
{
    private $taskClass;
    private $taskOpts;

    public function __construct($taskClass, array $taskOpts = [])
    {
        $this->taskClass = $taskClass;
        $this->taskOpts  = $taskOpts;
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        $class = new ReflectionClass($this->taskClass);
        $task  = $class->newInstance($this->taskOpts);

        return $task;
    }

    public function run()
    {
        $this->getTask()->run();
    }
}