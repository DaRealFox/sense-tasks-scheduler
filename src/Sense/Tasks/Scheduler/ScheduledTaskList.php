<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 24.09.2015
 * Time: 22:31
 */

namespace Sense\Tasks\Scheduler;


use DateTime;

interface ScheduledTaskList {

    public function getTasksDueAt(DateTime $dateTime);
}