<?php
namespace Sense\Tasks\Scheduler\TaskList;

use Sense\Tasks\Scheduler\Schedule\ScheduleParser;
use Sense\Tasks\Scheduler\ScheduledTaskList;

abstract class FileTaskList implements ScheduledTaskList
{
    /**
     * @var string
     */
    protected $file;

    /**
     * @param string $file
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * @return ScheduleParser
     */
    public function getParser()
    {
        return new ScheduleParser();
    }
}