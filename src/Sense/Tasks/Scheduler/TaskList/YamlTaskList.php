<?php
namespace Sense\Tasks\Scheduler\TaskList;

use DateTime;
use Sense\Tasks\LazyTask;
use Sense\Tasks\Scheduler\ScheduledTask;
use Sense\Tasks\TaskGroup;
use Symfony\Component\Yaml\Yaml;

class YamlTaskList extends FileTaskList
{
    /**
     * @return ScheduledTask[]
     */
    public function getTasks()
    {
        $config = Yaml::parse(file_get_contents($this->file));
        $tasks  = [];

        foreach($config['tasks'] as $taskName => $taskDef) {
            if($taskDef['enabled']) {
                $tasks[$taskName] = new ScheduledTask(
                    $this->getParser()->parseSchedule($taskDef['schedule']),
                    new LazyTask($taskDef['task']['class'], $taskDef['task']['options'])
                );
            }
        }

        return $tasks;
    }

    /**
     * @param DateTime $dateTime
     * @return TaskGroup
     */
    public function getTasksDueAt(DateTime $dateTime)
    {
        return new TaskGroup(array_filter($this->getTasks(), function(ScheduledTask $task) use($dateTime) {
            return $task->shouldRunAt($dateTime);
        }));
    }
}