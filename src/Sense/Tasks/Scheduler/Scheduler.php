<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 24.09.2015
 * Time: 22:29
 */

namespace Sense\Tasks\Scheduler;


use DateTime;
use Sense\Tasks\Runner\ParallelTaskRunner;
use Sense\Tasks\Runner\SequentialTaskRunner;
use Sense\Tasks\TaskRunner;

class Scheduler {
    /**
     * @var ScheduledTaskList
     */
    private $taskList;

    /**
     * @var TaskRunner
     */
    private $taskRunner;

    /**
     * @param ScheduledTaskList $taskList
     */
    public function __construct(ScheduledTaskList $taskList)
    {
        $this->taskList   = $taskList;
        $this->taskRunner = new ParallelTaskRunner();
    }

    /**
     * @param DateTime $dateTime
     */
    public function runTasksDueAt(DateTime $dateTime)
    {
        $this->taskRunner->runTasks($this->taskList->getTasksDueAt($dateTime));
    }
}