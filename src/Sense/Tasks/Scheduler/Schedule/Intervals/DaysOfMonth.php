<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 24.09.2015
 * Time: 21:46
 */

namespace Sense\Tasks\Scheduler\Schedule\Intervals;


use DateTime;
use Sense\Tasks\Scheduler\Schedule\Intervals\Base\Interval;

class DaysOfMonth extends DaysOfWeek
{
    private $occurence;

    /**
     * @param Interval $interval
     * @param int $occurence
     */
    public function __construct(Interval $interval, $occurence = 1)
    {
        parent::__construct($interval, 'w', 0, 7);
        $this->occurence = $occurence;
    }

    public function containsDateTime(DateTime $dateTime)
    {
        return parent::containsDateTime($dateTime)
            && (int) ceil($dateTime->format('j') / 7) === $this->occurence;
    }

    public function __tostring()
    {
        return parent::__tostring() . '#' . $this->occurence;
    }
}