<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 24.09.2015
 * Time: 20:31
 */

namespace Sense\Tasks\Scheduler\Schedule\Intervals\Base;


/**
 * Class Range
 *
 * @package Sense\Tasks\Scheduler\Intervals
 */
class Range extends Interval
{
    private $start;
    private $end;

    public function __construct($start, $end)
    {
        if($start > $end) {
            throw new \InvalidArgumentException(
                "start must be greater then end"
            );
        }

        $this->start = $start;
        $this->end   = $end;
    }

    /**
     * @param int $min
     * @param int $max
     * @return int[]
     */
    public function getPoints($min, $max)
    {
        return range(max($min, $this->start), min($max, $this->end));
    }

    public function contains($point, $min, $max)
    {
        #echo "  $point >= $min && $point <= $max && $point >= $this->start && $point <= $this->end\n";
        return $point >= $min && $point <= $max && $point >= $this->start && $point <= $this->end;
    }

    public function __tostring()
    {
        return $this->start . '-' . $this->end;
    }
}