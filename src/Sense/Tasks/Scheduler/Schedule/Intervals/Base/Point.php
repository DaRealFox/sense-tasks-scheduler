<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 24.09.2015
 * Time: 20:36
 */

namespace Sense\Tasks\Scheduler\Schedule\Intervals\Base;

/**
 * Class Point
 * @package Sense\Tasks\Scheduler\Intervals
 */
class Point extends Interval
{
    /**
     * @var int
     */
    private $point;

    /**
     * @param int $point
     */
    public function __construct($point)
    {
        $this->point = $point;
    }

    /**
     * @param int $min
     * @param int $max
     * @return int[]
     */
    public function getPoints($min, $max)
    {
        if($this->point >= $min && $this->point <= $max) {
            return [$this->point];
        }

        return [];
    }

    /**
     * @param int $point
     * @param $min
     * @param $max
     * @return bool
     */
    public function contains($point, $min, $max)
    {
        #echo "  $point === $this->point\n";
        return $point === $this->point;
    }

    public function __tostring()
    {
        return (string) $this->point;
    }
}