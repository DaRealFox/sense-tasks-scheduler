<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 24.09.2015
 * Time: 20:11
 */

namespace Sense\Tasks\Scheduler\Schedule\Intervals\Base;

/**
 * Class Interval
 *
 * @package Sense\Tasks\Scheduler\Intervals
 */
abstract class Interval
{
    /**
     * Returns all points between $min and $max that are covered by this
     * interval.
     *
     * @param int $min
     * @param int $max
     * @return int[]
     */
    public abstract function getPoints($min, $max);

    /**
     * Checks if this interval contains the given point.
     *
     * @param int $point
     * @param $min
     * @param $max
     * @return bool
     */
    public function contains($point, $min, $max)
    {
        return $point >= $min
            && $point <= $max
            && in_array($point, $this->getPoints($min, $max), true);
    }
}