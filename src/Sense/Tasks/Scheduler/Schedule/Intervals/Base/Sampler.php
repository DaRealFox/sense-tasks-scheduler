<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 24.09.2015
 * Time: 20:40
 */

namespace Sense\Tasks\Scheduler\Schedule\Intervals\Base;


class Sampler extends Interval
{
    /**
     * @var Interval
     */
    private $interval;

    /**
     * @var int
     */
    private $frequency;

    /**
     * @param Interval $interval
     * @param int $frequency
     */
    public function __construct(Interval $interval, $frequency)
    {
        $this->interval = $interval;
        $this->frequency = $frequency;
    }

    /**
     * @param int $min
     * @param int $max
     * @return int[]
     */
    public function getPoints($min, $max)
    {
        return array_filter($this->interval->getPoints($min, $max), function($v) {
            return $v % $this->frequency === 0;
        });
    }

    /**
     * @param int $point
     * @param $min
     * @param $max
     * @return bool
     */
    public function contains($point, $min, $max)
    {
        #echo "  $point % $this->frequency === 0\n";
        return $this->interval->contains($point, $min, $max)
            && $point % $this->frequency === 0;
    }

    public function __tostring()
    {
        return $this->interval . '/' . $this->frequency;
    }
}