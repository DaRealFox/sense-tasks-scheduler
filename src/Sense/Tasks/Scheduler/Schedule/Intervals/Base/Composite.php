<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 24.09.2015
 * Time: 21:30
 */

namespace Sense\Tasks\Scheduler\Schedule\Intervals\Base;


class Composite extends Interval {
    /**
     * @var Interval[]
     */
    private $intervals = [];

    public function __construct(array $intervals = [])
    {
        $this->intervals = $intervals;
    }

    /**
     * @param Interval $x
     */
    public function add(Interval $x) {
        $this->intervals[] = $x;
    }

    /**
     * @param int $min
     * @param int $max
     * @return int[]
     */
    public function getPoints($min, $max) {
        $points = [];

        foreach($this->intervals as $x) {
            $points = array_merge($points, $x->getPoints($min, $max));
        }

        return $points;
    }

    /**
     * @param int $point
     * @param $min
     * @param $max
     * @return bool
     */
    public function contains($point, $min, $max)
    {
        #echo "  $point >= $min && $point <= $max\n";
        if($point >= $min && $point <= $max) {
            foreach($this->intervals as $interval) {
                if($interval->contains($point, $min, $max)) {
                    return true;
                }
            }
        }

        return false;
    }

    public function __tostring()
    {
        return implode(',', $this->intervals);
    }
}