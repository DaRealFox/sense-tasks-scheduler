<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 24.09.2015
 * Time: 20:29
 */

namespace Sense\Tasks\Scheduler\Schedule\Intervals\Base;

/**
 * Class Always
 *
 * @package Sense\Tasks\Scheduler\Intervals
 */
class Always extends Interval
{
    /**
     * @param int $min
     * @param int $max
     * @return int[]
     */
    public function getPoints($min, $max)
    {
        return range($min, $max);
    }

    /**
     * @param int $point
     * @param $min
     * @param $max
     * @return bool
     */
    public function contains($point, $min, $max)
    {
        #echo "  $point >= $min && $point <= $max\n";
        return $point >= $min && $point <= $max;
    }

    public function __tostring()
    {
        return '*';
    }
}