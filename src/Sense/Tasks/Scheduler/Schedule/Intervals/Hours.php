<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 24.09.2015
 * Time: 21:45
 */

namespace Sense\Tasks\Scheduler\Schedule\Intervals;

use Sense\Tasks\Scheduler\Schedule\Intervals\Base\Interval;

/**
 * Class Hours
 * @package Sense\Tasks\Scheduler\Intervals
 */
class Hours extends TimeInterval
{
    /**
     * @param Interval $interval
     */
    public function __construct(Interval $interval)
    {
        parent::__construct($interval, 'H', 0, 23);
    }
}