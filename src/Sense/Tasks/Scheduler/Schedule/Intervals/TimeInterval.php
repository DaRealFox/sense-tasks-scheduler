<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 24.09.2015
 * Time: 21:42
 */

namespace Sense\Tasks\Scheduler\Schedule\Intervals;

use DateTime;
use Sense\Tasks\Scheduler\Schedule\Intervals\Base\Interval;

abstract class TimeInterval {

    /**
     * @var Interval
     */
    protected $interval;

    /**
     * @param Interval $interval
     * @param string $format
     * @param int $min
     * @param int $max
     */
    protected function __construct(Interval $interval, $format, $min, $max)
    {
        $this->interval = $interval;
        $this->format = $format;
        $this->min = $min;
        $this->max = $max;
    }

    /**
     * @param DateTime $dateTime
     * @return bool
     */
    public function containsDateTime(DateTime $dateTime)
    {
        return $this->containsPoint((int) $dateTime->format($this->format));
    }

    /**
     * @return string
     */
    public function __tostring()
    {
        return (string) $this->interval;
    }

    /**
     * @param int $point
     * @return bool
     */
    protected function containsPoint($point)
    {
        #echo get_class($this) . "::contains($point)\n";
        return $this->interval->contains($point, $this->min, $this->max);
    }
}