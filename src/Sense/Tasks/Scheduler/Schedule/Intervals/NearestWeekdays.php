<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 24.09.2015
 * Time: 21:45
 */

namespace Sense\Tasks\Scheduler\Schedule\Intervals;

use DateTime;
use Sense\Tasks\Scheduler\Schedule\Intervals\Base\Interval;

/**
 * Class Days
 *
 * This time interval is used for the W modifier of the days field. If
 * used, the nearest weekday to the given day is used to execute the
 * tasks if the day fell on a weekend.
 *
 * @package Sense\Tasks\Scheduler\Intervals
 */
class NearestWeekdays extends Days
{
    /**
     * @param Interval $interval
     */
    public function __construct(Interval $interval)
    {
        parent::__construct($interval, 'd', 1, 31);
    }

    /**
     * @param DateTime $dateTime
     * @return bool
     */
    public function containsDateTime(DateTime $dateTime)
    {
        $weekday = clone $dateTime;

        if(!parent::containsDateTime($weekday)) {
            if((int) $dateTime->format('w') === 1) {
                $weekday->modify('-1day');
            }

            else if((int) $dateTime->format('w') === 5) {
                $weekday->modify('+1day');
            }

            return $this->containsPoint((int) $weekday->format($this->format));
        }

        return true;
    }

    /**
     * @return string
     */
    public function __tostring() {
        return parent::__tostring() . 'W';
    }
}