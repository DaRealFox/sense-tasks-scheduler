<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 24.09.2015
 * Time: 21:46
 */

namespace Sense\Tasks\Scheduler\Schedule\Intervals;


use Sense\Tasks\Scheduler\Schedule\Intervals\Base\Interval;

class DaysOfWeek extends TimeInterval
{
    /**
     * @param Interval $interval
     */
    public function __construct(Interval $interval)
    {
        parent::__construct($interval, 'w', 0, 7);
    }
}