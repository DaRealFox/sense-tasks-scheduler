<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 24.09.2015
 * Time: 21:44
 */

namespace Sense\Tasks\Scheduler\Schedule\Intervals;


use Sense\Tasks\Scheduler\Schedule\Intervals\Base\Interval;

class Minutes extends TimeInterval
{
    /**
     * @param Interval $interval
     */
    public function __construct(Interval $interval)
    {
        parent::__construct($interval, 'i', 0, 59);
    }
}