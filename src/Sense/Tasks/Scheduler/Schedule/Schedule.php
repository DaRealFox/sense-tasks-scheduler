<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 24.09.2015
 * Time: 21:50
 */

namespace Sense\Tasks\Scheduler\Schedule;


use DateInterval;
use DatePeriod;
use DateTime;
use Generator;

use Sense\Tasks\Scheduler\Schedule\Intervals\Base\Always;
use Sense\Tasks\Scheduler\Schedule\Intervals\Days;
use Sense\Tasks\Scheduler\Schedule\Intervals\Hours;
use Sense\Tasks\Scheduler\Schedule\Intervals\Minutes;
use Sense\Tasks\Scheduler\Schedule\Intervals\Months;
use Sense\Tasks\Scheduler\Schedule\Intervals\DaysOfWeek;

class Schedule
{
    /**
     * @var Minutes
     */
    private $minutes;

    /**
     * @var Hours
     */
    private $hours;

    /**
     * @var Days
     */
    private $days;

    /**
     * @var Months
     */
    private $months;

    /**
     * @var
     */
    private $weekdays;

    /**
     * @param Minutes $minutes
     * @param Hours $hours
     * @param Days $days
     * @param Months $months
     * @param DaysOfWeek $weekdays
     */
    public function __construct(Minutes $minutes = null, Hours $hours = null, Days $days = null, Months $months = null, DaysOfWeek $weekdays = null)
    {
        $this->minutes  = $minutes ?: new Minutes(new Always());
        $this->hours    = $hours ?: new Hours(new Always());
        $this->days     = $days ?: new Days(new Always());
        $this->months   = $months ?: new Months(new Always());
        $this->weekdays = $weekdays ?: new DaysOfWeek(new Always());
    }

    /**
     * @param DateTime $start
     * @param DateTime $end
     * @return Generator|DateTime[]
     */
    public function getRunTimes(DateTime $start = null, DateTime $end = null)
    {
        $from = $start ?: new DateTime('now');
        $till = $end ?: new DateTime('now +1year');

        // Adjust the seconds
        $from->setTime($from->format('H'), $from->format('i'));

        foreach(new DatePeriod($from, new DateInterval('PT1M'), $till) as $minute) {
            if($this->shouldRunAt($minute)) {
                yield $minute;
            }
        }
    }

    /**
     * @param DateTime $start
     * @return DateTime
     */
    public function getNextRunTime(DateTime $start = null)
    {
        $dt = $start ?: new DateTime();

        foreach($this->getRunTimes($dt) as $next) {
            if($next > $dt) {
                return $next;
            }
        }

        return null;
    }

    /**
     * Checks if the schedule should run at the given time.
     *
     * @param DateTime $dateTime
     * @return bool
     */
    public function shouldRunAt(DateTime $dateTime)
    {
        return $this->minutes->containsDateTime($dateTime)
            && $this->hours->containsDateTime($dateTime)
            && $this->days->containsDateTime($dateTime)
            && $this->months->containsDateTime($dateTime)
            && $this->weekdays->containsDateTime($dateTime);
    }

    /**
     * @return bool
     */
    public function shouldRunNow()
    {
        return $this->shouldRunAt(new DateTime());
    }

    public function __tostring()
    {
        return implode(' ', [
            'i' => $this->minutes,
            'H' => $this->hours,
            'd' => $this->days,
            'm' => $this->months,
            'w' => $this->weekdays,
        ]);
    }
}