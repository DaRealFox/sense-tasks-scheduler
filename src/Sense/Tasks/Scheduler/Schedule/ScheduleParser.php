<?php
namespace Sense\Tasks\Scheduler\Schedule;

use InvalidArgumentException;
use Sense\Tasks\Scheduler\Schedule\Intervals\Base\Always;
use Sense\Tasks\Scheduler\Schedule\Intervals\Base\Composite;
use Sense\Tasks\Scheduler\Schedule\Intervals\Base\Interval;
use Sense\Tasks\Scheduler\Schedule\Intervals\Base\Point;
use Sense\Tasks\Scheduler\Schedule\Intervals\Base\Range;
use Sense\Tasks\Scheduler\Schedule\Intervals\Base\Sampler;
use Sense\Tasks\Scheduler\Schedule\Intervals\Days;
use Sense\Tasks\Scheduler\Schedule\Intervals\DaysOfMonth;
use Sense\Tasks\Scheduler\Schedule\Intervals\Hours;
use Sense\Tasks\Scheduler\Schedule\Intervals\Minutes;
use Sense\Tasks\Scheduler\Schedule\Intervals\Months;
use Sense\Tasks\Scheduler\Schedule\Intervals\DaysOfWeek;
use Sense\Tasks\Scheduler\Schedule\Intervals\NearestWeekdays;

class ScheduleParser
{
    public function __construct(array $options = [])
    {
        $this->presets = [
            '@hourly' => new Hourly(),
            '@daily'  => new Daily(),
            #'@weekly' => new Weekly(),
            '@monthly' => new Monthly(),
            '@yearly'  => new Yearly()
        ];
    }

    public function parseSchedule($schedule)
    {
        if(isset($this->presets[$schedule])) {
            return $this->presets[$schedule];
        }

        $values    = array_slice(explode(' ', $schedule, 6), 0, 5);
        $intervals = [];
        $nextWeekDay = false;
        $nthDayOfMonth = -1;

        foreach($values as $index => $value) {
            if(strpos($value, ',') !== false) {
                # Split lists into parts
                $interval = new Composite();

                foreach(explode(',', $value) as $part) {
                    $interval->add($this->parseInterval($part));
                }
            }
            else {
                # Split W modifier from value
                if($index === 2 && substr($value, -1) === 'W') {
                    $nextWeekDay = true;
                    $value = substr($value, 0, -1);
                }

                else if($index === 4 && strpos($value, '#') !== false) {
                    list($value, $nthDayOfMonth) = explode('#', $value);
                }

                $interval = $this->parseInterval($value);
            }

            $intervals[] = $interval;
        }

        return new Schedule(
            isset($intervals[0]) ? new Minutes($intervals[0]) : null,
            isset($intervals[1]) ? new Hours($intervals[1]) : null,
            isset($intervals[2]) ? ($nextWeekDay ? new NearestWeekdays($intervals[2]) : new Days($intervals[2])) : null,
            isset($intervals[3]) ? new Months($intervals[3]) : null,
            isset($intervals[4]) ? ($nthDayOfMonth > 0 ? new DaysOfMonth($intervals[4], $nthDayOfMonth) : new DaysOfWeek($intervals[4])) : null
        );
    }

    /**
     * @param string $part
     * @return Interval
     */
    private function parseInterval($part)
    {
        $frequency = null;

        # Split frequency from values
        if(strpos($part, '/') !== false) {
            list($part, $frequency) = explode('/', $part);
        }

        if($part == '*') {
            $interval = new Always();
        }

        else if(ctype_digit($part)) {
            $interval = new Point((int) $part);
        }

        else if(strpos($part, '-') !== false) {
            list($start, $end) = explode('-', $part);
            $interval = new Range((int) $start, (int) $end);
        }
        else {
            throw new InvalidArgumentException("Invalid value: $part");
        }

        if($frequency !== null) {
            $interval = new Sampler($interval, (int) $frequency);
        }

        return $interval;
    }
}