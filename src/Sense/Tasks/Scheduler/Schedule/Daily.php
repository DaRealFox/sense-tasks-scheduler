<?php
namespace Sense\Tasks\Scheduler\Schedule;

use Sense\Tasks\Scheduler\Schedule\Intervals\Base\Point;
use Sense\Tasks\Scheduler\Schedule\Intervals\Hours;
use Sense\Tasks\Scheduler\Schedule\Intervals\Minutes;

class Daily extends Schedule
{
    public function __construct()
    {
        parent::__construct(new Minutes(new Point(0)), new Hours(new Point(0)));
    }
}