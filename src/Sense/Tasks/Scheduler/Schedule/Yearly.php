<?php
namespace Sense\Tasks\Scheduler\Schedule;

use Sense\Tasks\Scheduler\Schedule\Intervals\Base\Point;
use Sense\Tasks\Scheduler\Schedule\Intervals\Days;
use Sense\Tasks\Scheduler\Schedule\Intervals\Hours;
use Sense\Tasks\Scheduler\Schedule\Intervals\Minutes;
use Sense\Tasks\Scheduler\Schedule\Intervals\Months;

class Yearly extends Schedule
{
    public function __construct()
    {
        parent::__construct(
            new Minutes(new Point(0)),
            new Hours(new Point(0)),
            new Days(new Point(1)),
            new Months(new Point(1))
        );
    }
}