<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 24.09.2015
 * Time: 22:37
 */

namespace Sense\Tasks\Scheduler;


use DateTime;
use Sense\Tasks\Scheduler\Schedule\Schedule;
use Sense\Tasks\Task;

class ScheduledTask implements Task {

    public function __construct(Schedule $schedule, Task $task)
    {
        $this->schedule = $schedule;
        $this->task     = $task;
    }

    /**
     * @param DateTime $dateTime
     * @return bool
     */
    public function shouldRunAt(DateTime $dateTime = null)
    {
        return $this->schedule->shouldRunAt($dateTime ?: new DateTime());
    }

    /**
     * @param DateTime $dateTime
     * @return DateTime
     */
    public function getNextRunTime(DateTime $dateTime = null) {
        return $this->schedule->getNextRunTime($dateTime);
    }

    public function run()
    {
        $this->task->run();
    }
}