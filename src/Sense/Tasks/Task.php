<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 27.09.2015
 * Time: 21:35
 */

namespace Sense\Tasks;


interface Task
{
    public function run();
}