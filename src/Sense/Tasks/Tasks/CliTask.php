<?php
namespace Sense\Tasks\Tasks;

use Sense\Tasks\Task;

class CliTask implements Task
{
    public function __construct(array $options)
    {
        $this->command = $options['command'];
    }

    public function run()
    {
        shell_exec($this->command);
    }
}